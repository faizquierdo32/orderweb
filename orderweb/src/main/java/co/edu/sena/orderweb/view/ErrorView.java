/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.view;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

/**
 * fecha:08/08/2022
 * @author Aprendiz
 * Objectivo: capturar la excepcion para el error 500
 */

public class ErrorView implements Serializable {

    /**
     * Creates a new instance of ErrorView
     */
    public ErrorView() {
    }
    public String printStackTrace(Throwable exception){
        StringWriter strinWriter = new StringWriter();
        exception.printStackTrace(new PrintWriter(strinWriter,true));
        return strinWriter.toString();
    }
    
}
