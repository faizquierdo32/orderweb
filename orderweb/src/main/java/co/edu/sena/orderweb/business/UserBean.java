/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.UserLogin;
import co.edu.sena.orderweb.persistence.IUserDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


/**
 *
 * @author Aprendiz
 */
@Stateless
public class UserBean implements UserBeanLocal {

    @EJB
    private IUserDAO userDAO;

    @Override
    public UserLogin findById(String username) throws Exception {
        if(username.isEmpty())
        {
            throw new Exception("El nombre de usuario es obligatorio");
        }
        
        return userDAO.findById(username);
    }

    @Override
    public List<UserLogin> findAll() throws Exception {
        return userDAO.findAll();
    }

    @Override
    public UserLogin login(String role, String password) throws Exception {
                
        if(role.isEmpty())
        {
            throw new Exception("El nombre de usuario es obligatorio");
        }
        //consulto si existe un usuario con el nombre ingresado en el login
        UserLogin oldUser = userDAO.findByRole(role);
        if(oldUser == null)
        {
            throw new Exception("Usuario incorrecto!");            
        }       
        
        if(!oldUser.getPassword().equals(password))
        {
            throw new Exception("Usuario incorrecto!"); 
        }
        
        return oldUser;
    }   
    
    
    
}
