/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.persistence;


import co.edu.sena.orderweb.model.Technician;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ITechnicianDAO {
    public void insert(Technician technician) throws Exception;
    public void update(Technician technician) throws Exception;
    public void delete(Technician technician) throws Exception;
    public Technician findById(Long document) throws Exception;
    public List<Technician> findAll() throws Exception;
}
