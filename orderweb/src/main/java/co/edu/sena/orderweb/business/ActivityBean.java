/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.orderweb.business;

import co.edu.sena.orderweb.model.Activity;
import co.edu.sena.orderweb.persistence.IActivityDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ActivityBean implements ActivityBeanLocal {

    @EJB
    private IActivityDAO activityDAO;
    
    public void validate(Activity activity) throws Exception
    {
        if(activity == null)
        {
            throw new Exception("La actividad es nula");
        }
        
        if(activity.getIdActivity() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        if(activity.getDescription().isEmpty())
        {
            throw new Exception("La descripción es obligatoria");
        }
        
        if(activity.getHours() == 0)
        {
            throw new Exception("Las horas son obligatorias");
        }
        
        if(activity.getDate() == null)
        {
            throw new Exception("La fecha es obligatoria");
        }
    }
    
    @Override
    public void insert(Activity activity) throws Exception {
        validate(activity);
        Activity oldActivity = activityDAO.findById(activity.getIdActivity());
        if(oldActivity != null)
        {
            throw new Exception("Ya existe una actividad con el mismo Id");
        }
        
        activityDAO.insert(activity);
    }

    @Override
    public void update(Activity activity) throws Exception {
        validate(activity);
        Activity oldActivity = activityDAO.findById(activity.getIdActivity());
        if(oldActivity == null)
        {
            throw new Exception("No existe una actividad con ese Id");
        }
        
        oldActivity.setDate(activity.getDate());
        oldActivity.setDescription(activity.getDescription());
        oldActivity.setHours(activity.getHours());
        oldActivity.setIdTechnician(activity.getIdTechnician());
        oldActivity.setIdType(activity.getIdType());      
        
        activityDAO.update(oldActivity);
    }

    @Override
    public void delete(Activity activity) throws Exception {
        if(activity == null)
        {
            throw new Exception("La actividad es nula");
        }
        
        if(activity.getIdActivity() == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        Activity oldActivity = activityDAO.findById(activity.getIdActivity());
        if(oldActivity == null)
        {
            throw new Exception("No existe una actividad con ese Id");
        }
        
        activityDAO.delete(oldActivity);
    }

    @Override
    public Activity findById(Integer idActivity) throws Exception {
        if(idActivity == 0)
        {
            throw new Exception("El Id es obligatorio");
        }
        
        return activityDAO.findById(idActivity);
    }

    @Override
    public List<Activity> findAll() throws Exception {
        return activityDAO.findAll();
    }

    
}
