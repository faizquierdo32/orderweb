/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.orderweb.view;

import co.edu.sena.orderweb.business.CausalBeanLocal;
import co.edu.sena.orderweb.model.Causal;
import co.edu.sena.orderweb.utils.MessageUtils;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Aprendiz
 */
public class CausalView {

    private InputText txtIdCausal;
    private InputText txtDescription;
    
    private CommandButton btnModificar;
    private CommandButton btnCrear;
    private CommandButton btnEliminar;
    
    private List<Causal> listCausal = null;
    
    @EJB
    private CausalBeanLocal causalBean;
    
    /**
     * Creates a new instance of CasualView
     */
    public CausalView() {
    }

    public InputText getTxtIdCausal() {
        return txtIdCausal;
    }

    public void setTxtIdCausal(InputText txtIdCausal) {
        this.txtIdCausal = txtIdCausal;
    }

    public InputText getTxtDescription() {
        return txtDescription;
    }

    public void setTxtDescription(InputText txtDescription) {
        this.txtDescription = txtDescription;
    }

    public CommandButton getBtnModificar() {
        return btnModificar;
    }

    public void setBtnModificar(CommandButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public CommandButton getBtnCrear() {
        return btnCrear;
    }

    public void setBtnCrear(CommandButton btnCrear) {
        this.btnCrear = btnCrear;
    }

    public CommandButton getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(CommandButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public List<Causal> getListCausal() {
         
        if (listCausal == null) {
            try {
                listCausal = causalBean.findAll();
            } catch (Exception e) {
                MessageUtils.addErrorMessage(e.getMessage());
            }
        }
        return listCausal;
    }

    public void setListCausal(List<Causal> listCausal) {
        this.listCausal = listCausal;
    }
    public void clear() {

        txtIdCausal.setValue("");
        txtDescription.setValue("");        

        listCausal = null;

        btnCrear.setDisabled(false);
        btnModificar.setDisabled(true);
        btnEliminar.setDisabled(true);

    }

    public void insert() {
        try {
            Causal causal = new Causal();
    
            causal.setDescription(txtDescription.getValue().toString());
            
            causalBean.insert(causal);
            
            MessageUtils.addInfoMessage("Causal creado exitosamente");
            clear();

        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void update() {
        try {
            Causal causal = new Causal();

            causal.setIdCausal(Integer.parseInt(txtIdCausal.getValue().toString()));
            causal.setDescription(txtDescription.getValue().toString());
            
            causalBean.update(causal);
            
            MessageUtils.addInfoMessage("Causal modificado exitosamente");
            clear();

        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void delete() {
        try {
            Causal causal = new Causal();
            causal.setIdCausal(Integer.parseInt(txtIdCausal.getValue().toString()));
            causalBean.delete(causal);
     
            MessageUtils.addInfoMessage("Causal eliminado exitosamente");
            clear();

        } catch (Exception e) {
             MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void onRowSelect(SelectEvent event) {

        Causal causal = (Causal) event.getObject();
        txtIdCausal.setValue(causal.getIdCausal());
        txtDescription.setValue(causal.getDescription());                
        
        //BTN
        btnCrear.setDisabled(true);
        btnModificar.setDisabled(false);
        btnEliminar.setDisabled(false);
    }

    public void txtIdListener() {
        try {
            Causal causal = causalBean.findById(Integer.parseInt(txtIdCausal.getValue().toString()));

            if (causal != null) {
                
                txtDescription.setValue(causal.getDescription());

                btnCrear.setDisabled(true);
                btnModificar.setDisabled(false);
                btnEliminar.setDisabled(false);
                
            } else {
                txtDescription.setValue("");
                
                btnCrear.setDisabled(false);
                btnModificar.setDisabled(true);
                btnEliminar.setDisabled(true);
                MessageUtils.addInfoMessage("Causal no registrado");
                
            }
        } catch (Exception e) {
           MessageUtils.addErrorMessage(e.getMessage());
        }
    }
    
}
