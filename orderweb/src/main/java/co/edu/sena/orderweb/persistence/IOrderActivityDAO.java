/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.OrderActivity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IOrderActivityDAO {
    public void insert(OrderActivity orderActivity) throws Exception;
    public void update(OrderActivity orderActivity) throws Exception;
    public void delete(OrderActivity orderActivity) throws Exception;
    public OrderActivity findById(Integer idOrder, Integer idActivity) throws Exception;
    public List<OrderActivity> findAll() throws Exception;
}
