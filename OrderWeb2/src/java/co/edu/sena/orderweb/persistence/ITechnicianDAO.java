/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Technician;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ITechnicianDAO {
    public void insert(Technician technician) throws Exception;
    public void update(Technician technician) throws Exception;
    public void delete(Technician technician) throws Exception;
    public Technician findById(Integer numero_formulario) throws Exception;
    public List<Technician> findAll() throws Exception;
}
