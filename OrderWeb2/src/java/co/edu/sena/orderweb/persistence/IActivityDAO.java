/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Activity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IActivityDAO {
    public void insert(Activity activity) throws Exception;
    public void update(Activity activity) throws Exception;
    public void delete(Activity activity) throws Exception;
    public Activity findById(Integer numero_formulario) throws Exception;
    public List<Activity> findAll() throws Exception;
}
