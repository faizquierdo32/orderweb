/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.TypeActivity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ITypeActivityDAO {
    public void insert(TypeActivity typeActivity) throws Exception;
    public void update(TypeActivity typeActivity) throws Exception;
    public void delete(TypeActivity typeActivity) throws Exception;
    public TypeActivity findById(Integer numero_formulario) throws Exception;
    public List<TypeActivity> findAll() throws Exception;
}
