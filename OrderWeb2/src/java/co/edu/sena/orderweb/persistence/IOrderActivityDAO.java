/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.OrderActivity;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IOrderActivityDAO {
    public void insert(OrderActivity orderActivity) throws Exception;
    public void update(OrderActivity orderActivity) throws Exception;
    public void delete(OrderActivity orderActivity) throws Exception;
    public OrderActivity findById(Integer numero_formulario) throws Exception;
    public List<OrderActivity> findAll() throws Exception;
}
