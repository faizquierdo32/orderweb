/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.orderweb.persistence;

import co.edu.sena.orderweb.model.Observation;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface IObservationDAO {
    public void insert(Observation observation) throws Exception;
    public void update(Observation observation) throws Exception;
    public void delete(Observation observation) throws Exception;
    public Observation findById(Integer numero_formulario) throws Exception;
    public List<Observation> findAll() throws Exception;
}
