package co.edu.sena.censoweb.model;

import co.edu.sena.censoweb.model.Activity;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2022-08-22T12:21:28")
@StaticMetamodel(Technician.class)
public class Technician_ { 

    public static volatile SingularAttribute<Technician, String> especiality;
    public static volatile SingularAttribute<Technician, String> phone;
    public static volatile SingularAttribute<Technician, Long> document;
    public static volatile SingularAttribute<Technician, String> name;
    public static volatile CollectionAttribute<Technician, Activity> activityCollection;

}