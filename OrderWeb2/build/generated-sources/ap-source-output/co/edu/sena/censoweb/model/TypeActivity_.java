package co.edu.sena.censoweb.model;

import co.edu.sena.censoweb.model.Activity;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2022-08-22T12:21:28")
@StaticMetamodel(TypeActivity.class)
public class TypeActivity_ { 

    public static volatile SingularAttribute<TypeActivity, Integer> idType;
    public static volatile SingularAttribute<TypeActivity, String> description;
    public static volatile CollectionAttribute<TypeActivity, Activity> activityCollection;

}