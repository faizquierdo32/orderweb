package co.edu.sena.censoweb.model;

import co.edu.sena.censoweb.model.Activity;
import co.edu.sena.censoweb.model.Order;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2022-08-22T12:21:28")
@StaticMetamodel(OrderActivity.class)
public class OrderActivity_ { 

    public static volatile SingularAttribute<OrderActivity, Activity> activity;
    public static volatile SingularAttribute<OrderActivity, Integer> serial;
    public static volatile SingularAttribute<OrderActivity, Order> order1;

}