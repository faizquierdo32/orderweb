package co.edu.sena.censoweb.model;

import co.edu.sena.censoweb.model.Order;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2022-08-22T12:21:28")
@StaticMetamodel(Observation.class)
public class Observation_ { 

    public static volatile CollectionAttribute<Observation, Order> order1Collection;
    public static volatile SingularAttribute<Observation, Integer> idObservation;
    public static volatile SingularAttribute<Observation, String> description;

}