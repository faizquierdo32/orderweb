package co.edu.sena.censoweb.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2022-08-22T12:21:28")
@StaticMetamodel(Order.class)
public class Order_ { 

    public static volatile SingularAttribute<Order, Date> legalizationDate;
    public static volatile SingularAttribute<Order, Integer> idOrder;
    public static volatile SingularAttribute<Order, String> address;
    public static volatile SingularAttribute<Order, String> city;
    public static volatile SingularAttribute<Order, String> state;

}