package co.edu.sena.censoweb.model;

import co.edu.sena.censoweb.model.OrderActivity;
import co.edu.sena.censoweb.model.Technician;
import co.edu.sena.censoweb.model.TypeActivity;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2022-08-22T12:21:28")
@StaticMetamodel(Activity.class)
public class Activity_ { 

    public static volatile SingularAttribute<Activity, Date> date;
    public static volatile SingularAttribute<Activity, Integer> hours;
    public static volatile SingularAttribute<Activity, TypeActivity> idType;
    public static volatile SingularAttribute<Activity, String> description;
    public static volatile CollectionAttribute<Activity, OrderActivity> orderActivityCollection;
    public static volatile SingularAttribute<Activity, Integer> idActivity;
    public static volatile SingularAttribute<Activity, Technician> idTechnician;

}