package co.edu.sena.censoweb.model;

import co.edu.sena.censoweb.model.Order;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.0.v20170811-rNA", date="2022-08-22T12:21:28")
@StaticMetamodel(Causal.class)
public class Causal_ { 

    public static volatile CollectionAttribute<Causal, Order> order1Collection;
    public static volatile SingularAttribute<Causal, String> description;
    public static volatile SingularAttribute<Causal, Integer> idCausal;

}